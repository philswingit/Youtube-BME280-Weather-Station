
#include <U8g2lib.h>
#include <U8x8lib.h>
#include <SPI.h>
#include <Wire.h>
#include <RFM69.h>
//#include <SD.h>

#define NETWORKID     100  // The same on all nodes that talk to each other
#define NODEID        1    // The unique identifier of this node

#define FREQUENCY     RF69_915MHZ
#define ENCRYPTKEY    "sampleEncryptKey"

U8X8_SSD1306_128X64_NONAME_SW_I2C u8x8(D2, D1, D3);
RFM69 radio = RFM69(D0, D8, true, D8);

typedef struct {
  uint16_t id;
  float temperature;
  float pressure;
  float humidity;
} __attribute__ ((packed)) packet;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

  /*if (!SD.begin()) {
    Serial.println("initialization failed!");
    return;
  }*/
  
  u8x8.setI2CAddress(0x078);
  u8x8.begin();
  u8x8.setPowerSave(0);
  
  radio.initialize(FREQUENCY,NODEID,NETWORKID);
  radio.setHighPower();
  radio.setPowerLevel(31); // power output ranges from 0 (5dBm) to 31 (20dBm)
  radio.encrypt(ENCRYPTKEY);

  u8x8.setFont(u8x8_font_pxplustandynewtv_f);
  u8x8.draw2x2String(4,3,"link");
}

bool havePacket = false;

void loop() {
  // put your main code here, to run repeatedly:
  if (radio.receiveDone() ){
    packet * p = (packet*) radio.DATA;
    Serial.println("Got Message");
    Serial.println(radio.DATALEN);
    Serial.println(p->id);
    Serial.println(p->temperature);
    Serial.println(p->pressure);
    Serial.println(p->humidity);
    
    if (!havePacket){
      u8x8.clearDisplay(); 
      havePacket = true;
    }
    
    u8x8.setCursor(1,3);
    u8x8.print("T:  ");
    u8x8.print(p->temperature);
    u8x8.drawUTF8(13, 3, "ºC");

    u8x8.setCursor(1,4);
    u8x8.print("H:  ");
    u8x8.print(p->humidity);
    u8x8.drawString(13, 4, "%");
    
    u8x8.setCursor(1,5);
    u8x8.print("P: ");
    u8x8.print(p->pressure);
    u8x8.drawString(13, 5, "hPA");

    u8x8.drawString(15,7, "."); 
     if (radio.ACKRequested())
      {
        radio.sendACK();
        Serial.println(" - ACK sent");
      }
    /*
    File log = SD.open("W1.txt", FILE_WRITE);
    if (log){
      log.print(p.temperature,4);
      log.print(",");
      log.print(p.pressure, 4);
      log.print(",");
      log.print(p.altitude, 4);
      log.print(",");
      log.println(p.humidity, 4);
      log.close();
    } else {
      u8x8.drawString(15,7, "x"); 
    }
    */
    delay(1000);
    u8x8.drawString(15,7, " "); 
  }
  radio.receiveDone();
}
