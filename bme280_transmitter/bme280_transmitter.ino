

/***************************************************************************
  This is a library for the BME280 humidity, temperature & pressure sensor

  Designed specifically to work with the Adafruit BME280 Breakout
  ----> http://www.adafruit.com/products/2650

  These sensors use I2C or SPI to communicate, 2 or 4 pins are required
  to interface.

  Adafruit invests time and resources providing this open source code,
  please support Adafruit andopen-source hardware by purchasing products
  from Adafruit!

  Written by Limor Fried & Kevin Townsend for Adafruit Industries.
  BSD license, all text above must be included in any redistribution
 ***************************************************************************/

#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
#include <RFM69.h>
 
#define NETWORKID     100  // The same on all nodes that talk to each other
#define NODEID        2    // The unique identifier of this node
#define RECEIVER      1    // The recipient of packets

#define FREQUENCY     RF69_915MHZ
#define ENCRYPTKEY    "sampleEncryptKey"
 
//*********************************************************************************************

Adafruit_BME280 bme; // I2C

RFM69 radio = RFM69(10, 2, true, 0);

typedef struct {
  uint16_t id;
  double temperature;
  double pressure;
  double humidity;
} packet;

uint16_t packetnum = 0;  // packet counter

void setup() {
  Serial.begin(9600);
  Serial.println(F("BME280 test"));

  if (!bme.begin(0x76)) {
    Serial.println("Could not find a valid BME280 sensor, check wiring!");
    while (1);
  }

  // initialize radio
  radio.initialize(FREQUENCY,NODEID,NETWORKID);
  radio.setHighPower();
  radio.setPowerLevel(31); // power output ranges from 0 (5dBm) to 31 (20dBm)
  radio.encrypt(ENCRYPTKEY);

}

packet p;
void loop() {
    p.id          = packetnum++;
    p.temperature = bme.readTemperature();
    p.pressure    = bme.readPressure() / 100.0F;
    p.humidity    = bme.readHumidity();
    
    Serial.print("Temperature = ");
    Serial.print(p.temperature);
    Serial.println(" *C");

    Serial.print("Pressure = ");

    Serial.print(p.pressure);
    Serial.println(" hPa");


    Serial.print("Humidity = ");
    Serial.print(p.humidity);
    Serial.println(" %");

    Serial.println();

    Serial.println("Trying to send");
    Serial.println(sizeof(packet));
    if (radio.sendWithRetry(RECEIVER, &p, sizeof(packet))) { 
      Serial.println("OK");
    }
    
    delay(5000);
}
